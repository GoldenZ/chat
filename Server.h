#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <thread>
#include <vector>
#include "Helper.h"

class Server
{
public:
	Server();
	~Server();
	void serve();

private:
	std::string users;
	std::vector<std::thread> threads;

	void accept();
	void clientHandler(SOCKET clientSocket);

	void logIn(SOCKET clientSocket);

	SOCKET _serverSocket;
};
