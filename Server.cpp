#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <fstream>
#include <Ws2tcpip.h>

using namespace std;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve()
{
	struct sockaddr_in sa = { 0 };
	string line;
	ifstream file("config.txt");
	string ip = "";
	string port = "";

	getline(file, line);
	ip = line.substr(10);
	getline(file, line);
	port = line.substr(5);
	cout << ip <<endl;

	sa.sin_port = htons(stoi(port, NULL)); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	InetPton(AF_INET, ip.c_str(), &(sa.sin_addr));
	//sa.sin_addr.s_addr = INADDR_ANY; // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		//threads.push_back(std::thread(&Server::accept, Server()));
		//std::thread th(&Server::accept, this);
		
		//accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	clientHandler(client_socket);
}


void Server::clientHandler(SOCKET clientSocket)
{
	try
	{
		int code = 0;

		while (true)
		{
			code = Helper::getMessageTypeCode(clientSocket);
			
			switch (code)
			{
			case MT_CLIENT_LOG_IN: // 200 login
				logIn(clientSocket);

			default:
				break;
			}
		}

		// Closing the socket (in the level of the TCP protocol)
		closesocket(clientSocket);
	}
	catch (const std::exception & e)
	{
		closesocket(clientSocket);
	}
}

void Server::logIn(SOCKET clientSocket)
{
	int len = Helper::getIntPartFromSocket(clientSocket, 2);
	string name = Helper::getStringPartFromSocket(clientSocket, len);
	string msg = "";
	
	if (this->users.size() == 0)
	{
		this->users.append(name);
	}
	else
	{
		this->users.append("&");
		this->users.append(name);
	}

	Helper::send_update_message_to_client(clientSocket, "", "", this->users);
}